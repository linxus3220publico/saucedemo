#Autor: Miguel Alvarez
@Compraexitosa

  Feature: Compra SWAGLABS

    Scenario: Compra exitos
      Given Que un user ingresa sus credenciales en la pagina de saucedemo
      |srtUser      |srtPassword |
      |standard_user|secret_sauce|
      When Seleccione el producto con el menor precio
      Then realizara la orden/pedido de manera exitosa
      |strNombre|strApellido|strZip|srtTextoFinal           |
      |Miguel   |Alvarez    |700001|THANK YOU FOR YOUR ORDER|