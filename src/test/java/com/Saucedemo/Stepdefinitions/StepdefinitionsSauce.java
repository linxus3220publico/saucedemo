package com.Saucedemo.Stepdefinitions;

import com.Saucedemo.model.SauceDatos;
import com.Saucedemo.questions.Respuesta;
import com.Saucedemo.tasks.AbrirSaucePagina;
import com.Saucedemo.tasks.DiligenciarLogin;
import com.Saucedemo.tasks.RealizarCompra;
import com.Saucedemo.tasks.SeleccionarArticulo;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.*;

public class StepdefinitionsSauce {

    @Before
    public void setOnStage(){
        setTheStage(new OnlineCast());
    }

    @Given("^Que un user ingresa sus credenciales en la pagina de saucedemo$")
    public void que_un_user_ingresa_sus_credenciales_en(List<SauceDatos> datos){
        theActorCalled("user").wasAbleTo(
                AbrirSaucePagina.enLaPagina()
        );
        theActorInTheSpotlight().attemptsTo(
                DiligenciarLogin.enLaPagina(datos)
        );
    }


    @When("^Seleccione el producto con el menor precio$")
    public void seleccione_el_producto_con_el_menor_precio(){
        theActorInTheSpotlight().attemptsTo(
                SeleccionarArticulo.enLaPagina()
        );
    }

    @Then("^realizara la orden/pedido de manera exitosa$")
    public void realizara_la_orden_pedido_de_manera_exitosa(List<SauceDatos> datos) {
        theActorInTheSpotlight().attemptsTo(
                RealizarCompra.enLaPagina(datos)
        );
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(Respuesta.es(datos)));
    }

}
