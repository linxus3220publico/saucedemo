package com.Saucedemo.Runner;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions (features = "src/test/resources/feature/Saucedemo.feature",
        glue = "com.Saucedemo.Stepdefinitions",
        snippets = SnippetType.CAMELCASE )

public class RunnerTags {
}
