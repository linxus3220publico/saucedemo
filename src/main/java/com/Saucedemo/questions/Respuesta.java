package com.Saucedemo.questions;

import com.Saucedemo.model.SauceDatos;
import com.Saucedemo.userinterfaces.PaginaCompra;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;


import java.util.List;

public class Respuesta implements Question<Boolean> {
    private List<SauceDatos> datos;

    public Respuesta(List<SauceDatos> datos) {
        this.datos = datos;
    }

    public static Respuesta es(List<SauceDatos> datos) {
        return new Respuesta(datos);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        String texto_mensaje = Text.of(PaginaCompra.TEXTO_COMPRA).viewedBy(actor).asString();
        return  datos.get(0).getSrtTextoFinal().equals(texto_mensaje);
    }
}
