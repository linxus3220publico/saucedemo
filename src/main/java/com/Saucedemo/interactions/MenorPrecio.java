package com.Saucedemo.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.Saucedemo.userinterfaces.PaginaArticulos.BOTON_PRODUCTO;
import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getDriver;

public class MenorPrecio implements Interaction {

    public static  MenorPrecio checking (){
        return Tasks.instrumented(MenorPrecio.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        ArrayList<Float> fListElements = new ArrayList<Float>();

        List<WebElement> webElementListPrice = getDriver().findElements(By.xpath("//*[@id='inventory_container']//div[@class='inventory_item_price']"));
        for (WebElement webElementPrice: webElementListPrice){
            fListElements.add(Float.parseFloat(webElementListPrice.getText ().substring(1)));
        }
        Collections.sort(fListElements);
        System.out.println("Primero: "+fListElements.get(0));

        actor.attemptsTo(
                Click.on(BOTON_PRODUCTO.of(fListElements.get(0).toString()))
        );


    }
}
