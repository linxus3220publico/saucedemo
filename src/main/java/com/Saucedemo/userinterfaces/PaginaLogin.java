package com.Saucedemo.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PaginaLogin {

    public static final Target INPUT_USER = Target.the("Usuario").located(By.id("user-name"));
    public static final Target INPUT_PASSWORD = Target.the("Password").located(By.id("password"));
    public static final Target BOTON_LOGIN = Target.the("Boton Login").located(By.id("login-button"));

}





