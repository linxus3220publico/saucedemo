package com.Saucedemo.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PaginaArticulos {
    public static final Target OPCION1 = Target.the("OPCION1").located(By.xpath("/html/body/div/div/div/div[2]/div/div/div/div[1]/div[2]/div[2]/div"));
    public static final Target OPCION2 = Target.the("OPCION2").located(By.xpath("/html/body/div/div/div/div[2]/div/div/div/div[2]/div[2]/div[2]/div"));
    public static final Target OPCION3 = Target.the("OPCION3").located(By.xpath("/html/body/div/div/div/div[2]/div/div/div/div[3]/div[2]/div[2]/div"));
    public static final Target OPCION4 = Target.the("OPCION4").located(By.xpath("/html/body/div/div/div/div[2]/div/div/div/div[4]/div[2]/div[2]/div"));
    public static final Target OPCION5 = Target.the("OPCION5").located(By.xpath("/html/body/div/div/div/div[2]/div/div/div/div[5]/div[2]/div[2]/div"));
    public static final Target BOTON_PRODUCTO = Target.the("OPCION6").located(By.xpath("/html/body/div/div/div/div[2]/div/div/div/div[6]/div[2]/div[2]/div"));
    public static final Target BOTON_AGREGAR_CARRITO = Target.the("Agregar al carrito").located(By.id("add-to-cart-sauce-labs-onesie"));
    public static final Target BOTON_CARRITO = Target.the("Ir al carrito").located(By.id("shopping_cart_container"));


}

