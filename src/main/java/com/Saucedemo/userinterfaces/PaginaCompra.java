package com.Saucedemo.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PaginaCompra {
    public static final Target BOTON_CHECKOUT = Target.the("Boton checkout").located(By.id("checkout"));
    public static final Target INPUT_NOMBRE = Target.the("Primer nombre").located(By.id("first-name"));
    public static final Target INPUT_APELLIDO = Target.the("Apellido").located(By.id("last-name"));
    public static final Target INPUT_ZIP = Target.the("Codigo postal").located(By.id("postal-code"));
    public static final Target BOTON_CONTINUAR = Target.the("Continuar").located(By.id("continue"));
    public static final Target BOTON_FINALIZAR = Target.the("Finalizar compra").located(By.id("finish"));
    public static final Target TEXTO_COMPRA = Target.the("Mensaje de compra finalizada").located(By.xpath("/html/body/div/div/div/div[2]/h2"));
}
