package com.Saucedemo.model;

public class SauceDatos {
    private String srtUser;
    private String srtPassword;
    private String strNombre;
    private String strApellido;
    private String strZip;
    private String srtTextoFinal;

    public String getStrText() {
        return strText;
    }

    public void setStrText(String strText) {
        this.strText = strText;
    }

    private String strText;
    public String getSrtUser() {
        return srtUser;
    }

    public void setSrtUser(String srtUser) {
        this.srtUser = srtUser;
    }

    public String getSrtPassword() {
        return srtPassword;
    }

    public void setSrtPassword(String srtPassword) {
        this.srtPassword = srtPassword;
    }

    public String getStrNombre() {
        return strNombre;
    }

    public void setStrNombre(String strNombre) {
        this.strNombre = strNombre;
    }

    public String getStrApellido() {
        return strApellido;
    }

    public void setStrApellido(String strApellido) {
        this.strApellido = strApellido;
    }

    public String getStrZip() {
        return strZip;
    }

    public void setStrZip(String strZip) {
        this.strZip = strZip;
    }

    public String getSrtTextoFinal() {
        return srtTextoFinal;
    }

    public void setSrtTextoFinal(String srtTextoFinal) {
        this.srtTextoFinal = srtTextoFinal;
    }

}
