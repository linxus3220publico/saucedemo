package com.Saucedemo.tasks;


import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static com.Saucedemo.userinterfaces.PaginaArticulos.BOTON_AGREGAR_CARRITO;
import static com.Saucedemo.userinterfaces.PaginaArticulos.BOTON_CARRITO;

public class SeleccionarArticulo implements Task {

    public static SeleccionarArticulo enLaPagina() {
        return Tasks.instrumented(SeleccionarArticulo.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BOTON_AGREGAR_CARRITO),
                Click.on(BOTON_CARRITO)
        );

    }
}
