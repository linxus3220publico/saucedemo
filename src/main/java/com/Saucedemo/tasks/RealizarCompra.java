package com.Saucedemo.tasks;

import com.Saucedemo.model.SauceDatos;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;

import static com.Saucedemo.userinterfaces.PaginaCompra.*;

public class RealizarCompra implements Task {

    private List<SauceDatos> datos;

    public RealizarCompra(List<SauceDatos> datos) {
        this.datos = datos;
    }

    public static RealizarCompra enLaPagina(List<SauceDatos> datos) {
        return Tasks.instrumented(RealizarCompra.class, datos);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BOTON_CHECKOUT),
                Enter.theValue(datos.get(0).getStrNombre()).into(INPUT_NOMBRE),
                Enter.theValue(datos.get(0).getStrApellido()).into(INPUT_APELLIDO),
                Enter.theValue(datos.get(0).getStrZip()).into(INPUT_ZIP),
                Click.on(BOTON_CONTINUAR),
                Click.on(BOTON_FINALIZAR)

        );

    }
}
