package com.Saucedemo.tasks;

import com.Saucedemo.model.SauceDatos;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;

import static com.Saucedemo.userinterfaces.PaginaLogin.*;

public class DiligenciarLogin implements Task {
    private List<SauceDatos> datos;

    public DiligenciarLogin(List<SauceDatos> datos) {
        this.datos = datos;
    }

    public static DiligenciarLogin enLaPagina(List<SauceDatos> datos) {
        return Tasks.instrumented(DiligenciarLogin.class,datos);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(datos.get(0).getSrtUser()).into(INPUT_USER),
                Enter.theValue(datos.get(0).getSrtPassword()).into(INPUT_PASSWORD),
                Click.on(BOTON_LOGIN)

        );

    }
}
