package com.Saucedemo.tasks;

import com.Saucedemo.userinterfaces.PaginaSauce;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirSaucePagina implements Task {
    private PaginaSauce paginaSauce;

    public static AbrirSaucePagina enLaPagina() {
        return Tasks.instrumented(AbrirSaucePagina.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn(paginaSauce)
        );

    }
}
